import gulp from 'gulp'
import browserSync from 'browser-sync'
import pug from 'gulp-pug'

const server = function(cb) {
    browserSync.init({
        watch: true,
        server: {
            baseDir: "./public/",
        },
        port: 9999
    })

    cb()
}

const compilePug = function(cb) {
    gulp.src('./private/views/**/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('./public/'))

    cb()
}

const watch = function(cb) {
    gulp.watch('./private/**/*.pug', compilePug)
    cb()
}


exports.default = gulp.series(
    compilePug,
    server,
    watch
)