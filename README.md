
# Documentation serveur local avec gulp et browser-sync

### Creation du repertoire 

Creer un repertoire dans le dossier de votre choix, exemple **www/local-server**


```
mkdir www
cd www
mkdir local-server
cd local-server
```


###  Initialisation de NPM

Afin de pouvoir utiliser les packages NPM, il faut lancer une initialisation du repertoire
```
npm init
```
Suivez les différentes étapes de l'installation (vous pouvez spammer entree pour l'exemple)

### Installation de gulp

Nous allons donc utiliser gulp pour cet exemple, en mode ES6 pour plus de clarté de code :
```
npm i -D gulp @babel/register @babel/preset-env
```

Pour pouvoir utiliser la syntaxe ES6, il faut également créer un fichier **.babelrc** a la racine du projet et copier/coller le code suivant :
```
{
    "presets": ["@babel/preset-env"]
}

```

### Test d'une fonction gulp

On va maintenant créer notre premiere fonction gulp afin d'être sur que l'installation se soit bien passé.

Créer un fichier **gulpfile.babel.js** à la racine du projet :
```
import gulp from 'gulp'

const test = function(cb) {
    console.log("voici ma premiere fonction")
    cb()
}

const test = function(cb) {
    console.log("ma deuxieme fonction")
    cb()
}

exports.default = gulp.series(
    test,
    test2
)
```

### Installation de Browser Sync
```
npm i -D browser-sync
```

On va modifier le fichier `gulpfile.babel.js` pour initialiser un serveur simple avec du liveReload :
```
import gulp from 'gulp'
import browserSync from 'browser-sync'

const server = function(cb) {
    browserSync.init({
        watch: true,
        server: {
            baseDir: "./public/",
        },
        port: 9999
    })

    cb()
}

exports.default = gulp.series(
    server
)
```

Et on va créer un fichier `index.html` dans un dossier `public` :

```
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Titre de la page</title>
  </head>
  <body>
    <p>Lorem ipsum dolor sit amet consectetur adipisicinge elit. Tempore maiores a, reprehenderit eligendi dolore dignissimos asperiores laborum voluptates illo, blanditiis exercitationem, illum fuga. Dolore perspiciatis a blanditiis, vitae eveniet odit.</p>
  </body>
</html>
```

### Aller plus loin avec la gestion de Pug

Installation du package de gestion de pug par Gulp :
`npm i -D gulp-pug`

On ajoute ensuite la compilation du pug dans le fichier `gulpfile.babel.js` en créant une tâche, que l'on ajoutera a la tâche principale :

```
import gulp from 'gulp'
import browserSync from 'browser-sync'
import pug from 'gulp-pug'

const server = function(cb) {
    browserSync.init({
        watch: true,
        server: {
            baseDir: "./public/",
        },
        port: 9999
    })

    cb()
}

const compilePug = function(cb) {
    gulp.src('./private/views/**/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('./public/'))

    cb()
}

const watch = function(cb) {
    gulp.watch('./private/**/*.pug', compilePug)
    cb()
}


exports.default = gulp.series(
    compilePug,
    server,
    watch
)
```

Voici des exemple de fichiers pug :
**private/layouts/base.pug**
```
block config
doctype html
html(lang='fr')
  head
    meta(charset='UTF-8')
    //if IE
      meta(http-equiv='X-UA-Compatible' content='IE=edge')
    meta(name='viewport' content='width=device-width, initial-scale=1.0')
    title #{titre}
    link(rel='stylesheet' href='css/styles.css' media='all')
  body
    block content
```
**private/views/index.pug**
```
extends ../layouts/base.pug

block config
  - titre = 'Titre de la page'

block content 
  p Lorem ipsum dolor sit amet consectetur adipisicinge elit. Tempore maiores a, reprehenderit eligendi dolore dignissimos asperiores laborum voluptates illo, blanditiis exercitationem, illum fuga. Dolore perspiciatis a blanditiis, vitae eveniet odit.
```
# Aller encore plus loin
- Documentation Gulp
- Documentation browser-sync

# Lancement du projet
```
npm i
npm run dev
```